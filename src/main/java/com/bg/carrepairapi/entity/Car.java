package com.bg.carrepairapi.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class Car {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 10)
    private String carNumber;

    @Column(nullable = false, length = 30)
    private String breakDown;

    @Column(nullable = false)
    private LocalDate breakDate;

    @Column(nullable = false, length = 30)
    private String repairDetails;

    @Column(nullable = false)
    private LocalDate endDate;

    @Column(nullable = false)
    private Double price;
}
