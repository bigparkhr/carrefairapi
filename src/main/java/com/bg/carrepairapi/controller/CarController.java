package com.bg.carrepairapi.controller;

import com.bg.carrepairapi.model.CarItem;
import com.bg.carrepairapi.model.CarRequest;
import com.bg.carrepairapi.service.CarService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/carRepair")
public class CarController {
    private final CarService carservice;

    @PostMapping("/new")
    public String setCar(@RequestBody CarRequest request){
        carservice.setCar(request);

        return "OK";
    }
    @GetMapping("/all")
    public List<CarItem> getCars() {return carservice.getCars(); }

}
