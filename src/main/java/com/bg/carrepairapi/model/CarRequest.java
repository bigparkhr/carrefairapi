package com.bg.carrepairapi.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class CarRequest {
    private String carNumber;
    private String breakDown;
    private LocalDate breakDate;
    private String repairDetails;
    private LocalDate endDate;
    private Double price;
}
