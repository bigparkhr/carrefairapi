package com.bg.carrepairapi.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class CarItem {
    private Long id;
    private String carNumber;
    private String breakDown;
    private LocalDate breakDate;
    private String repairDetails;
}
