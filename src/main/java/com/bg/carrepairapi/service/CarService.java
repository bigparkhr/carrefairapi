package com.bg.carrepairapi.service;

import com.bg.carrepairapi.entity.Car;
import com.bg.carrepairapi.model.CarItem;
import com.bg.carrepairapi.model.CarRequest;
import com.bg.carrepairapi.repository.CarRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CarService {
    private final CarRepository carRepository;

    public void setCar(CarRequest request) {
        Car addData = new Car();
        addData.setCarNumber(request.getCarNumber());
        addData.setBreakDown(request.getBreakDown());
        addData.setBreakDate(request.getBreakDate());
        addData.setRepairDetails(request.getRepairDetails());
        addData.setEndDate(request.getEndDate());
        addData.setPrice(request.getPrice());

        carRepository.save(addData);
    }

    public List<CarItem> getCars() {
        List<Car> originList = carRepository.findAll();

        List<CarItem> result = new LinkedList<>();

        for (Car car : originList) {
            CarItem addItem = new CarItem();
            addItem.setId(car.getId());
            addItem.setCarNumber(car.getCarNumber());
            addItem.setBreakDown(car.getBreakDown());
            addItem.setBreakDate(car.getBreakDate());
            addItem.setRepairDetails(car.getRepairDetails());

            result.add(addItem);
        }

        return result;
    }
}
